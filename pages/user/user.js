// pages/user/user.js

const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    objectArray : [
      { id: 5, unique: 'unique_5' },
      { id: 4, unique: 'unique_4' },
      { id: 3, unique: 'unique_3' },
      { id: 2, unique: 'unique_2' },
      { id: 1, unique: 'unique_1' },
      { id: 0, unique: 'unique_0' },
    ],
    item : {
      index:1024,
      name:'1024社区',
      time:'8012年'
    },
    logTitle : 'default'
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
      wx.showToast({
        title: '刷新完成',
      })
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
      return {
        title:"Kinglond's Product",
        path:"/pages/todo/todo",
        imageUrl:"/image/share/share1.png"
      }
  },

  sortByRandom : function() {
    const length = this.data.objectArray.length
    for (let i = 0; i < length - 1; i++) {
      // const x = Math.floor(Math.random() * length)
      // const y = Math.floor(Math.random() * length)
      const temp = this.data.objectArray[i]
      this.data.objectArray[i] = this.data.objectArray[i + 1]
      this.data.objectArray[i + 1] = temp
    }

    this.setData({
      objectArray : this.data.objectArray
    })
  },
  outterTap : function() {
    this.setData({
      logTitle : "outter"
    })
  },
  middleTap : function() {
    this.setData({
      logTitle: "middle"
    })
  },
  innerTap : function() {
    this.setData({
      logTitle: "inner"
    })
  }
})