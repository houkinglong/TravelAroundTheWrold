// pages/todo/todo.js

var oriMeters = 0.0;
var timer; // 计时器
var isSportStart = false;
var seconds = 0;

function startCountdown(that) {
  timer: setInterval(function () {

    if (!isSportStart) {
      wx.showToast({
          title: 'sss' + myCovers.length,
        })
      clearInterval(timer);
    }
    
    seconds += 1;
    if (seconds % 5 == 0) {
      that.getLocation();
    }

    var time = date_format(seconds);
    that.setData({
      time: time,
    })
  }
  ,1000)
}

function stopCountDown() {
  clearInterval(timer);
}

// 时间格式化输出
function date_format(seconds) {
  var hor = Math.floor(seconds / (60 * 60))
  var min = Math.floor(seconds / 60 - hor * 60)
  var sec = Math.floor(seconds - hor * 60 * 60 - min * 60)

  return hor + ":" + (min < 10 ? ("0" + min) : min) + ":" + (sec < 10 ? ("0" + sec) : sec) + " ";
}

function getDistance(lat1, lng1, lat2, lng2) {
  var dis = 0;
  var radLat1 = toRadians(lat1);
  var radLat2 = toRadians(lat2);
  var deltaLat = radLat1 - radLat2;
  var deltaLng = toRadians(lng1) - toRadians(lng2);
  var dis = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(deltaLat / 2), 2) + Math.cos(radLat1) * Math.cos(radLat2) * Math.pow(Math.sin(deltaLng / 2), 2)));
  return dis * 6378137;

  function toRadians(d) { return d * Math.PI / 180; }
} 

Page({

  /**
   * 页面的初始数据
   */
  data: {
    latitude: 23.099994,
    longitude: 113.324520,
    meters : 0,
    time: "0:00:00",
    markers: [],
    covers: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getLocation()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  getLocation: function() {
    var that = this
    wx.getLocation({
      type: 'gcj02',
      success: function(res) {
        var newCover = {
          latitude: res.latitude,
          longitude: res.latitude,
          icaonPath: '/image/redPoint.png'
        }

        var myCovers = that.data.covers
        var count = myCovers.length
        var lastCover
        if (count == 0) {
          myCovers.push(newCover)
        }

        count = myCovers.length
        lastCover = myCovers[count - 1]
        
        var newMeters = getDistance(newCover.latitude, newCover.longitude, lastCover.latitude, lastCover.longitude)/1000

        oriMeters += newMeters

        var meters = new Number(oriMeters)
        var showMeters = meters.toFixed(2)

        myCovers.push(newCover)
        
        that.setData({
          latitude: res.latitude,
          longitude: res.longitude,
          covers: myCovers,
          meters: showMeters,
        })
      }
    })
  },
  sportStart : function() {
    if (isSportStart) {
      return;
    }

    isSportStart = true;
    startCountdown(this);
    this.getLocation();
  },
  sportPause : function() {
    if (!isSportStart) {
      return;
    }
    wx.showToast({
      title: '运动暂停',
    })
    isSportStart = false;
    stopCountDown();
  }
})